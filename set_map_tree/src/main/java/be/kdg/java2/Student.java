package be.kdg.java2;

public class Student implements Comparable<Student> {
    public static int compareCounter = 0;
    public static int equalsCounter = 0;

    private int id;
    private String name;

    public Student(int id) {
        this.id = id;
        this.name = "";
    }

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(Student other) {
        compareCounter++;
        if (other.id == this.id) {
            return other.name.compareTo(this.name);
        } else {
            return other.id - this.id;
        }
    }

    @Override
    public boolean equals(Object o) {
        equalsCounter++;
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        if (id != student.id) return false;
        return name != null ? name.equals(student.name) : student.name == null;
    }

    @Override
    public int hashCode() {
        return 31 * id + name.hashCode();
    }

//    @Override
//    public int hashCode() {
//        return id + name.length();
//    }

//    @Override
//    public int hashCode() {
//        return 1;
//    }
}
